<h2>Giant Puzzle #6 – Formiga</h2>

Suponha que uma formiga esteja fazendo um passeio aleatório por um cubo situado no octante positivo. A probabilidade de ela partir de um vértice e chegar a qualquer outro vértice é inversamente proporcional à distância euclidiana entre eles. Ela parte da origem do sistema (0, 0, 0) e deseja chegar no vértice situado no extremo oposto, (1, 1, 1). Note que ela pode ir do vértice (0, 0, 0) ao (1, 1, 1) em apenas um passo.

a) Qual o valor esperado de passos para a formiga chegar ao vértice oposto?

b) Qual o valor esperado de passos para a formiga chegar ao vértice oposto, dado que o último vértice que a formiga atingiu antes de chegar ao extremo oposto é (1, 1, 0)?

c) Suponha que a formiga esteja fazendo agora um passeio aleatório por um hipercubo de dimensão 7, partindo da origem (0, 0, 0, 0, 0, 0, 0) e que seu objetivo seja chegar a (1, 1, 1, 1, 1, 1, 1). Qual seria o valor esperado de passos para chegar a tal vértice?
E se o vértice anterior ao último for (1, 1, 1, 1, 1, 1, 0)?

 

Observação: Forneça as respostas com pelo menos 6 casas de precisão.
